
PATH2MAPPINGS = '../../mappings_construction/all_tables_mappings.json'
PATH2FLATTENING = '/home/commun/echange/flattening_echantillon/'
#PATH2FLATTENING = '../../../../../x-dataInitiative/SNIIRAM-flattening/src/test/resources/flattening/parquet-table/'
PATH2OMOP_DB = '/home/commun/echange/matthieu_doutreligne/omop_database/'
#PATH2OMOP_DB = '/home/matthieu/Téléchargements/'
PATH2CDM_SCHEMA = '../../ressources/OMOP CDM postgresql ddl.txt'

PATH2PIVOT = '../../ressources/snds_nomenclatures_internes/MAPPER-SNDS-concept_pivot.csv'
