---
title: "Documentation on the mapping of the SNDS to the OMOP model"
date: 2019-07-17
---
# care_site

## DA_PRA_R

+ care_site_id : func.col('ACT_CAB_COD') if it exists, else: func.concat_ws('', func.lit('pfs_care_site_'), func.col('PFS_COD_POS'), func.col('PFS_PFS_NUM'))
+ place_of_service_source_value : func.col('PFS_PRA_SPE'), could find place_of_service from speciality ? ASK ANTOINE!!!!

## ER_ETE_F_joined_with_finess

+ care_site_id :  PB 'ETB_EXE_FIN' which is the geographic missing one caracter for the finess (9), a juridic finess can group several geographic (strange but here http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/Num%C3%A9ro_FINESS_juridique)
EDIT: we can solve this problem using 
the Finess referentiel `BE_IDE_R` or `DATASANTE_T_FINESS`
produced by Atlassante -> this can resolve all issues
dealing with location_id etc. 
mieux vaut utiliser la table atlassante car mise
à jour récemment sur les extractions finess mais il
faut l'extraire du portail
+ location_id : Should be linked with a Finess referentiel, so we have to link the whole dcir where this line is not blank with the finess file
EDIT: same remark here

## PMSI_E_joined_with_finess

+ place_of_service_concept_id : STA_ETA has only three values in the PMSI/SSR: STC, OQN, DGF which indicates that it is not a valable source of inforamtion for place of service concept, we should search this information in the complementary finess file. A part un mapping depuis le nom de l'etablissement je ne vois pas de solution. Thre is some info here http://finess.sante.gouv.fr/fininter/jsp/pdf.do?xsl=CategEta.xsl

# condition_occurrence

## HAD_B_DP

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786627, primary condition: how to include hierarchy (linking an etiology to a FPP)?

## HAD_D

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786629, secondary condition: how to include hierarchy (linking an etiology to a FPP)?

## IR_IMB_R

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_source_value : func.col('MED_MTF_COD') : Colonne non retrouvee, a verifier
+ condition_type_concept_id : 44786629, primary condition as adviced by A. Lammer et al. (Lille)

## MCO_B_DP

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_status_concept_id : 4230359, final diagnosis
+ condition_type_concept_id : 44786627, primary condition

## MCO_B_DR

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786629, secondary condition

## MCO_D

+ condition_occurrence_id : incremental, filter null lines when doing this -> temporary until id is found
+ condition_status_concept_id : func.lit(4230359) final diagnosis because all diagnoses of the SNDS are definitive diagnoses after the care is done
+ condition_type_concept_id : 44786629, secondary condition
+ provider_id : ETA_NUM_GEO is more precise, but might be difficult to map to a location description
+ visit_detail_id : could be used to store the 'UM' value ? -> Excellent: TODO: Find this info in MCO

## RIP_RSA

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786627, primary condition: how to include hierarchy (linking an etiology to a FPP)?

## RIP_RSAD

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786629, secondary condition: how to include hierarchy (linking an etiology to a FPP)?

## SSR_B_AE

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786629, secondary condition: how to include hierarchy (linking an etiology to a FPP)?

## SSR_B_MMP

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_status_source_value : Manifestation Morbide Principale (MMP): can be SNOMED or RxNorm code
+ condition_type_concept_id : 44786627, primary condition

## SSR_D

+ condition_occurrence_id : incremental, filter null lines when doing this
+ condition_type_concept_id : 44786629, final condition

# cost

## ER_PRS_F

+ ER_PRS_F : in DCIR
+ PRS_PAI_MNT : montant total signé de la dépense
+ billed_date : put date of prescription instead of date of beginning of treatment (check?)

## NS_PRS_F

+ ER_PRS_F : in DCIRS
+ PRS_PAI_MNT : montant global de la dépense
+ cost : corresponds to the sum of 'base de remboursement' + 'montant du dépassement d'honoraire', that we can also write : 'montant versé' + 'participation forfaitaire' + 'ticket modérateur' + 'montant dudépassement d'honoraires'
+ incurred date : date of beginning of treatment or prescription or hospit. Date of beginning of pregnancy could also be interesting given it's beginning of condition
+ paid_date : for now beginning of treatment

# drug_exposure

## ER_PHA_F

+ drug_type_concept_id : func.lit(38000175), Prescription dispensed in pharmacy
+ quantity : func.col('PHA_ACT_QSN') is the number of factured boxes (more appropriate than pha_dec_qsu, deconditionned quantity which is often 0)
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## ER_UCD_F

+ drug_type_concept_id : func.lit(581452/38000180) depending on UCD_TOP_UCD = 0 -> pharmacie retrocédée (outpatient)/ UCD_TOP_UCD = 1 -> pharmacie en sus du GHS (inpatient) (see http://open-data-assurance-maladie.ameli.fr/wiki-sniiram/index.php/UCD_TOP_UCD)
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ sig : func.col('ER_UCD_F__UCD_UCD_COD') not sure about this one

## HAD_FH

+ drug_exposure_start_date : DEL_DAT_ENT should be use to refine the start of the exposure
+ drug_type_concept_id : func.lit(43542358) physician dispensed drug (identified through EHR
+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## HAD_MED

+ PRS_TYP : I dont know what inforamtion is there, label writes type de prestation
+ drug_exposure_start_date : an information in the column DAT_DELAI could be used to refine the start date
+ drug_type_concept_id : func.lit(43542358) physician dispensed drug (identified through EHR
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp medicament en sus

## HAD_MEDATU

+ PRS_TYP : I dont know what information is there, label writes type de prestation
+ drug_exposure_start_date : an information in the column DAT_DELAI could be used to refine the start date
+ drug_type_concept_id : func.lit(43542358) physician dispensed drug (identified through EHR
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp medicament en sus

## MCO_CE__FHSTC

+ drug_type_concept_id : func.lit(581452) outpatient dispensed
+ provider_id : func.col('PFS_EXE_NUM'): might be null if done in establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## MCO_FH

+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## MCO_MED

+ PRS_TYP : I dont know what inforamtion is there, label writes type de prestation
+ drug_exposure_start_date : an information in the column DELAI could be used to refine the start date
+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp medicament en sus

## MCO_MEDATU

+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp médicament soumis à autorisation temporaire d'utilisation

## MCO_MEDTHROMBO

+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp Méd. thrombolytiques pour le traitement des AVC ischémiques 

## RIP_FH

+ Main comment : useless in our extraction: WE DONT HAVE ANY COD IN THE UCD_UCD_COD
+ drug_exposure_start_date : DEL_DAT_ENT should be use to refine the start of the exposure
+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## SSR_FH

+ drug_exposure_start_date : DEL_DAT_ENT should be use to refine the start of the exposure
+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.

## SSR_MED

+ PRS_TYP : I dont know what inforamtion is there, label writes type de prestation
+ drug_exposure_start_date : an information in the column DAT_DELAI could be used to refine the start date
+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp medicament en sus

## SSR_MEDATU

+ PRS_TYP : I dont know what information is there, label writes type de prestation
+ drug_exposure_start_date : an information in the column DAT_DELAI could be used to refine the start date
+ drug_type_concept_id : func.lit(38000180) inpatient dispensed
+ provider_id : func.col(ETA_NUM) should contain the information on the establishment
+ route_source_value : func.lit('ATC01') ATC01 could be used to indicate the source route value and then use an atc to snomed route mapping that should exists.
+ source_table_label : Fich comp medicament en sus

# location

## DA_PRA_R

+ county : when the zip code begins with 97, then the dpt code is given by the first 3 letters (see: https://fr.wikipedia.org/wiki/Code_postal_en_France#France_d'outre-mer)

## ER_ETE_F_joined_with_finess

+ address_1 : For all informations about the hospital we use the information located in https://www.data.gouv.fr/fr/datasets/finess-extraction-des-entites-juridiques/#community-resources (the cleaned 2015 version)

## ER_PRS_F_join_with_IR_GEO_R

+ BEN_RES_DPT_2d : created with IR_GEO_R to build 6 digits zip codes.
+ location_id : NOTA:l'identifiant est créé avec le code zip numerique et non pas avec les codes sources snds (string)

## IR_BEN_R_join_with_IR_GEO_R

+ BEN_RES_DPT_2d : created with IR_GEO_R to build 6 digits zip codes.
+ location_id : NOTA:l'identifiant est créé avec le code zip numerique et non pas avec les codes sources snds (string)

## MCO_B_join_with_IR_GEO_R

+ county : TODO
+ location_source_id : code postaux PMSI, les départements sont codés 2A, 2B pr la corse et 9A, 9B, 9C, 9D, 9F..., code géographique vers code commune insee
+ location_source_id_v2 : func.concat_ws('', func.col('MCO_B__BDI_DPT'), func.col('MCO_B__BDI_COD')).cast('string')
+ zip : TODO

## PMSI_E_joined_with_finess

+ address_1 : For all informations about the hospital we use the information located in https://www.data.gouv.fr/fr/datasets/finess-extraction-des-entites-juridiques/#community-resources (the cleaned 2015 version)
+ location_id : zip||ETA_NUM, following the strategies of minimal jointures rule (minJoin rule)

# location_history

## IR_BEN_R

+ location_history : used to track a patient history in all the snds, measurement are taken from IR_BEN_R and from ER_PRS_F where location is registred at each measurement
+ location_source_id : exceptions corse 209 pour le département; les codes 097 et , implémenter les tables de correction nationale (RFCOMMUN.CORRECTIONS_COM2012_NEW, RFCOMMUN.T_FIN_GEO_LOC_FRANCE)

## location_er_prs_f

+ location_er_prs_f : er_prs_f.select('NUM_ENQ', 'BEN_RES_COM', 'BEN_RES_DPT', 'EXE_SOI_DTD').orderBy('NUM_ENQ', 'EXE_SOI_DTD').groupBy('NUM_ENQ', 'BEN_RES_DPT', 'BEN_RES_COM').agg(
    func.first(func.col('EXE_SOI_DTD')).alias('start'),
    func.last(func.col('EXE_SOI_DTD')).alias('end'))

## location_mco_b

+ location_er_prs_f : er_prs_f.select('NUM_ENQ', 'BEN_RES_COM', 'BEN_RES_DPT', 'EXE_SOI_DTD').orderBy('NUM_ENQ', 'EXE_SOI_DTD').groupBy('NUM_ENQ', 'BEN_RES_DPT', 'BEN_RES_COM').agg(
    func.first(func.col('EXE_SOI_DTD')).alias('start'),
    func.last(func.col('EXE_SOI_DTD')).alias('end'))
+ location_source_id : code postaux PMSI, les départements sont codés 2A, 2B pr la corse et 9A, 9B, 9C, 9D, 9F..., code géographique vers code commune insee
+ location_source_id_v2 : func.concat_ws('', func.col('MCO_B__BDI_DPT'), func.col('MCO_B__BDI_COD')).cast('string')

# observation

## ER_PRS_F

+ CMU : social feature that describes if the patient is a beneficiary of the universal health complementray insurance 
+ observation_type_concept_id : 38000280, Observation recorded from EHR

# payer_plan_period

## ER_PRS_F

+ ER_PRS_F : in DCIR
+ ORG_AFF_BEN : organisme d'affiliation et non organisme de liquidation, c'est l'organisme où est affilié le bénéficiaire des soins
+ PRS_PAI_MNT : montant total signé de la dépense
+ billed_date : put date of prescription instead of date of beginning of treatment (check?)
+ contract_person_id : il faut coder cela en termes de clef, le probleme c'est qu'ici on a que le NUMENQ. Dans le portail SNDS de la CNAM on pourrait essayer de voir si la personne est l'ayant droit ou l'assuré cf. Présentation Claire Lise Dubost en fonction de la clef.

# person

## IR_BEN_R


# procedure_occurrence

## ER_CAM_F

+ modifier_source_value : what is the nature of these codes ????
+ private : how to distinguish between private and public ?, noticable is that private acts with ete are in double (in pmsi too)
+ procedure_date : EXE_SOI_DTD not in ER_CAM_F, but in ER_PRS_F, we use the flattened version of the DCIR
+ procedure_datetime : EXE_SOI_DTD not in ER_CAM_F, but in ER_PRS_F, we use the flattened version of the DCIR
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)
+ quantity : PRS_ACT_QTE not in ER_CAM_F, but in ER_PRS_F and NS_PRS_F, we use the flattened version of the DCIR

## HAD_A

+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## HAD_FM

+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## MCO_A

+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## MCO_CE__FMSTC

+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## MCO_FM

+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## RIP_FM

+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## SSR_B

+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## SSR_CCAM

+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## SSR_CE__FMSTC

+ procedure_datetime : DEL_DAT_ENT should be used to compute exact procedure date
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

## SSR_FM

+ private : In the Heva diagram, this is notaded as private care_site: these are the OQN (objectif quantifié national) which are some kinds of private care_site previously bound to the state with a quantitative objective
+ procedure_datetime : DEL_DAT_ENT should be used to compute exact procedure date
+ procedure_type_concept_id : func.lit(44786630), primary procedure, as adviced by A. Lammer et al. (Lille)

# provider

## DA_PRA_R

+ care_site_id : See if ACT_CAB_COD exists in data, func.concat_ws('', func.lit('pfs_care_site_'), func.col('PFS_COD_POS'), func.col('PFS_PFS_NUM')). DA_PRA_R__PFS_FIN_NUM ne semble pas interessant dans mes donnees car il map beaucoup a un code etablissement generique et dans le cas inverse, je n'arrive pas a savoir quel est le mapping, masi sinon on peut mapper avec
+ specialty_source_value : PFS_PRA_SPE est-il renseigne pour les natures d'activite egalement (specialite non medicales)?

# visit_detail

## ER_PRS_F

+ preceding_visit_occurrence_id : Could be useful for affiliated tables, second time

# visit_occurrence

## ER_PRS_F

+ care_site_id : func.col('ER_ETE_F__ETB_EXE_FIN'), NOT SURE AT ALL here: We dont have the finess of the providers (only there local identifier pfs_pfs_num). Most events in this table are made by a medical practitionner for which we dont have the finess. We could set the care_site_id to null except for lines where ER_ETE_F__ETB_EXE_FIN is not null (WARNing -> geographic finess and not juridic as in the pmsi..
+ discharge_to_concept_id : no value found in the dcir for mouvement codes, default could be home/home (8536 id for place of service) but it is false for some observations.
+ preceding_visit_occurrence_id : Could be computed afterwards by patient with a window function: visit_occurrence.groupBy('persond_id').select('visit_occurrence_id', 'visit_start_datetime')
+ visit_end_date : Maybe replace by start date if null afterwards
+ visit_type_concept_id : func.lit(32034), visit derived from EHR billing record, A. Lammer et al (Lille) put func.lit(44818517) visit derived from encounter on claim for all visit types

## HAD_B

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ provider_id : no pfs id in outpatient visits
+ visit_concept_id : func.lit(581476) home visit

## MCO_B

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ visit_concept_id : 9201, Inpatient by default, but we could catch emergency visits and other more fine grained than just inptient
+ visit_type_concept_id : func.lit(32035), visit derived from ehr encounter record, A. Lammer et al (Lille) put func.lit(44818517) visit derived from encounter on claim for all visit types

## MCO_FASTC

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ provider_id : Pas d'id du pfs dans les consultations externes...
+ visit_concept_id : func.lit('9202'), outpatient visit
+ visit_type_concept_id : func.lit(32035), visit derived from ehr encounter record, A. Lammer et al (Lille) put func.lit(44818517) visit derived from encounter on claim for all visit types

## RIP_RSA

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ visit_concept_id : func.lit(9201), inpatient visit

## SSR_B

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ visit_concept_id : func.lit('9201') for inpatient, but more fine grained could be catch
+ visit_type_concept_id : func.lit(32035), visit derived from ehr encounter record, A. Lammer et al (Lille) put func.lit(44818517) visit derived from encounter on claim for all visit types

## SSR_FASTC

+ preceding_visit_occurrence_id : Could be computed afterwards by patient avec une window function
+ provider_id : no pfs id in outpatient visits

