import json
import os
from typing import Dict
import pandas as pd


def individual_mapping_to_df(target_table_name: str, target_table_dict: Dict) -> pd.DataFrame:
    """
    Convert one mapping in a dictionnary format to a pd.Dataframe
    :param target_table_name:
    :param target_table_dict:
    :return:
    """
    target_table_list = []
    for source_table_name, source_table in target_table_dict['source_tables'].items():
        columns = [target_table_name + '__field', source_table_name + '__field',
                   source_table_name + '_comment']
        df = pd.DataFrame.from_dict(source_table['CDM_columns'], orient='index',
                                    columns=columns[1:2])
        df.reset_index(level=0, inplace=True)
        # add commentaries
        commentaries = []
        for col in source_table['CDM_columns'].keys():
            if col in source_table['comments'].keys():
                commentaries.append(source_table['comments'][col])
            else:
                commentaries.append('')
        df['source_table_commentary'] = commentaries
        df.columns = columns
        df.index = df[target_table_name + '__field']
        df.drop(target_table_name + '__field', axis=1, inplace=True)
        # df.columns = df.loc['target_field']
        target_table_list.append(df.transpose())
    target_table_df = pd.concat(target_table_list, axis=0, sort=False)
    return target_table_df


def all_mappings_to_df(path2mappings: str):
    """
    Convert all json mappings to a dictionnary of dataframe
    :param path2mappings:
    :return:
    """
    all_mappings_df = {}
    for mapping_file in os.listdir(path2mappings):
        # print(mapping_file)
        with open(os.path.join(path2mappings, mapping_file), 'r') as f:
            target_table_dict = json.load(f)
        target_table_name = target_table_dict['table']
        mapping_df = individual_mapping_to_df(target_table_name=target_table_name,
                                              target_table_dict=target_table_dict)
        all_mappings_df[target_table_name] = mapping_df

    return all_mappings_df


def all_mappings_to_csv(path2mappings: str, path2all_tables_mapping_csv):
    """
    write all inidividual mappings into one csv with one sheet per target table
    :param path2mappings:
    :param path2all_tables_mapping_csv:
    :return:
    """
    all_mappings_df = all_mappings_to_df(path2mappings)
    writer = pd.ExcelWriter(path2all_tables_mapping_csv, engine='xlsxwriter')
    workbook = writer.book
    # Add a header format.
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'center',
        'fg_color': 'ACADB5',
        'border': 1})

    for k, df in all_mappings_df.items():
        df.to_excel(writer, sheet_name=k)
        worksheet = writer.sheets[k]
        for col_num, value in enumerate(df.columns.values):
            # Write the column headers with the defined format.
            # worksheet.set_row(1, 2)
            worksheet.set_row(0, 20)  # Set the height of Row 1 to 20.
            worksheet.write(0, col_num + 1, value, header_format)
            # rewrite overwritten first cell
            worksheet.write(0, 0, k, header_format)
            # set column width
            # series = df[value]
            worksheet.set_column(col_num, col_num, 40)
            # freeze first row and first column
        bg_format1 = workbook.add_format({'bg_color': '#78B0DE'})  # blue cell background color
        bg_format2 = workbook.add_format({'bg_color': '#FFFFFF'})  # white cell background color
        for row in range(0, df.shape[0], 2):
            worksheet.set_row(row + 1, cell_format=bg_format1)
            worksheet.set_row(row + 2, cell_format=bg_format2)
            # worksheet.write(row, 0, "Hello")
            # worksheet.write(row + 1, 0, "world")
        worksheet.freeze_panes(1, 1)
    writer.save()
    writer.close()
    return


def union_mappings(path2mappings, path2all_tables_mapping):
    """concatenate all individual json mappings in one json"""
    all_tables_mappings = {'tables': dict()}
    for mapping_name in os.listdir(path2mappings):
        with open(os.path.join(path2mappings, mapping_name), 'r') as f:
            mapping = json.load(f)
        all_tables_mappings['tables'][mapping['table']] = mapping

    with open(path2all_tables_mapping, 'w') as f:
        json.dump(all_tables_mappings, f, indent=2, sort_keys=True)
    return


""" Deprecated
def json_to_commentaries(path2all_tables_mapping, path2commentaries, my_date):
    with open(path2all_tables_mapping, 'r') as f:
        mappings = json.load(f)['tables']

    print('Building documentation for the following tables: ')
    comments_text = '---\ntitle: "Documentation on the mapping of the SNDS to the OMOP model"\ndate: {}\n---\n'.format(
        my_date)

    for table_name, table in mappings.items():
        print(table['table'])
        comments_text += '# {}\n\n'.format(table_name)

        for source_name, source in table['source_tables'].items():
            print(source_name)
            comments_text += '## {}\n\n'.format(source_name)
            for variable, comment in source['comments'].items():
                comments_text += '+ {} : {}\n'.format(variable, comment)
            comments_text += '\n'

    with open(path2commentaries, 'w') as f:
        f.write(comments_text)
    return

def json_to_csv(path2all_tables_mapping, path2csv):
    with open(path2all_tables_mapping, 'r') as f:
        mappings = json.load(f)['tables']

    colnames = ['CDM_field', 'source_fields', 'CDM_table', 'source_table']
    all_tables = pd.DataFrame(columns=colnames)
    empty_row = pd.DataFrame(['-----', '-----', '-----', '-----']).transpose()
    empty_row.columns = colnames

    for cdm_table_name, cdm_table in mappings.items():
        print(cdm_table_name)
        for source_table_name, source_table in cdm_table['source_tables'].items():
            df = pd.DataFrame.from_dict(source_table['CDM_columns'], orient='index', columns=colnames[1:2])
            df.reset_index(level=0, inplace=True)
            df['cdm_table'] = cdm_table_name
            df['source_table'] = source_table_name
            df.columns = colnames
            all_tables = pd.concat((all_tables, empty_row, df))
        all_tables = pd.concat((all_tables, empty_row))

    all_tables.to_csv(path2csv, index=False)
    return
"""
